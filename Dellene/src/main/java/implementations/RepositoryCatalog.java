
package implementations;

import Repositories.IRepositoryCatalog;
import Repositories.IRepositoryPet;
import Repositories.IRepositoryPhoto;
import Repositories.IRepositoryUser;


public class RepositoryCatalog implements IRepositoryCatalog{

	private RepositoryDb db;
	
    public IRepositoryPhoto getPhotos() {
        return new RepositoryPhoto(db);
    }

 
    public IRepositoryPet getPets() {
    	return new RepositoryPet(db);
    }

    public IRepositoryUser getUsers() {
        return new RepositoryUser(db);
    }
    
}
