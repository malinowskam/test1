
/* wyszukiwanie usera poprzez np login, email, wiek, miasto itp */
package implementations;

import Repositories.IRepositoryUser;
import domain.User;
import java.util.List;
import java.util.ArrayList;

public class RepositoryUser implements IRepositoryUser{

	private RepositoryDb db;
	
	public RepositoryUser(RepositoryDb db)
	{
		super();
		this.db = db;
	}
	

    public User get(int id) {
    	for(User u : db.users)
    	{
    		if(u.getID()==id)
    			return u;
    	}
        return null;
    }

    public List<User> getAll() {
    	return db.users;
    }
    
    public void add(User entity) {
    	db.users.add(entity);
    }

    public void delete(User entity) {
        db.users.remove(entity);
    }

    public void update(User entity) {

    }
    
    
    
    public User searchByLogin(String login) {
        for(User u : db.users)
    	{
    		if(u.getLogin()==login)
    			return u;
    	}
        return null;
    }

    public User searchByEmail(String email) {
        for(User u : db.users)
    	{
    		if(u.getEmail()==email)
    			return u;
    	}
        return null;  
    }

    public List<User> searchByName(String name) {
        List<User> result = new ArrayList<User>();
        for (User u : db.users)
             if (u.getName() == name) result.add(u);
            return result;
    }

    public List<User> searchByCity(String city) {
        List <User> result = new ArrayList<User>();
        for (User u : db.users)
             if (u.getCity() == city) result.add(u);
            return result;
    }

    public List<User> searchByAge(int age) {
         List <User> result = new ArrayList<User>();
        for (User u : db.users)
             if (u.getAge() == age) result.add(u);
            return result;        
    }

    public List<User> searchBySurname(String surname) {
         List <User> result = new ArrayList<User>();
        for (User u : db.users)
             if (u.getSurname() == surname) result.add(u);
            return result;
    }
    
}
