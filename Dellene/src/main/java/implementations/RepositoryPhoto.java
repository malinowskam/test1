/*
 Dodawanie description i Date do listy aby mo�na by�o ich szuka�
 */

package implementations;

import Repositories.IRepositoryPhoto;
import domain.Photo;
import java.util.List;
import java.util.ArrayList;

public class RepositoryPhoto implements IRepositoryPhoto{

	private RepositoryDb db;

	public RepositoryPhoto(RepositoryDb db)
	{
		super();
		this.db = db;
	}
	
    public Photo get(int id) {
    	for(Photo ph : db.photos)
    	{
    		if(ph.getID()==id)
    			return ph;
    	}
        return null;
    }

    public List<Photo> getAll() {
        return db.photos;
    }

    public void add(Photo entity) {
        db.photos.add(entity);
    }

    public void delete(Photo entity) {
        db.photos.remove(entity);
    }

    public void update(Photo entity) {
        
    }
   
    
    
    
    public List<Photo> searchByDescription(String description) {
        List<Photo> result = new ArrayList<Photo>();
            for (Photo ph : db.photos)
                if (ph.getDescription() == description) result.add(ph);
            return result;
    }

    public List<Photo> searchByDate(int date) {
        List<Photo> result = new ArrayList<Photo>();
            for (Photo ph : db.photos)
                if (ph.getDate() == date) result.add(ph);
            return result;
    }
    
}
