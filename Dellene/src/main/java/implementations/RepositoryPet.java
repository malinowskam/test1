
package implementations;

import Repositories.IRepositoryPet;
import domain.Pet;
import java.util.List;
import java.util.ArrayList;



public class RepositoryPet implements IRepositoryPet {

	private RepositoryDb db;

	public RepositoryPet(RepositoryDb db)
	{
		super();
		this.db = db;
	}
	
    public Pet get(int id) {
    	for(Pet p : db.pets)
    	{
    		if(p.getID()==id)
    			return p;
    	}
        return null;
    }

    public List<Pet> getAll() {
        return db.pets;
    }

    public void add(Pet entity) {
        db.pets.add(entity);
    }

    public void delete(Pet entity) {
    	 db.pets.remove(entity);
    }

    public void update(Pet entity) {
        
    }
    
    
    public List<Pet> searchByName(String name) {
        List<Pet> result = new ArrayList<Pet>();
            for (Pet p : db.pets)
                if (p.getName() == name) result.add(p);
            return result;
    }

    public List<Pet> searchByAge(int age) {
        List<Pet> result = new ArrayList<Pet>();
        for (Pet p : db.pets)
             if (p.getAge() == age) result.add(p);
            return result;
        
    }

    public List<Pet> searchByBreed(String breed) {
        List<Pet> result = new ArrayList<Pet>();
        for (Pet p : db.pets)
             if (p.getBreed() == breed) result.add(p);
            return result;
    }

    public List<Pet> searchBySpecies(String species) {
        List<Pet> result = new ArrayList<Pet>();
        for (Pet p : db.pets)
             if (p.getSpecies() == species) result.add(p);
            return result;
    }
    
}
