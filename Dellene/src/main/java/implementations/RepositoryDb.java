package implementations;

import java.util.*;
import domain.*;

public class RepositoryDb {

	public List<User> users;
	public List<Photo> photos;
	public List<Pet> pets;
	
	public RepositoryDb()
	{
		users  = new ArrayList<User>();
		photos = new ArrayList<Photo>();
		pets   = new ArrayList<Pet>();
	}
	
}
