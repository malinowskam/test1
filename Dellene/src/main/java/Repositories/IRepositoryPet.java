/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Repositories;

import java.util.List;
import domain.Pet;

public interface IRepositoryPet extends IRepository<Pet>{
    
List<Pet> searchByName(String name);
List<Pet> searchByAge(int age);
List<Pet> searchByBreed(String breed);
List<Pet> searchBySpecies(String species);
    
}
