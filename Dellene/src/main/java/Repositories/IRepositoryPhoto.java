/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Repositories;

import java.util.List;
import domain.Photo;

public interface IRepositoryPhoto extends IRepository<Photo>{
    
List<Photo> searchByDescription(String description);
List<Photo> searchByDate(int date);
    
}
