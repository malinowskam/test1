/*wyszukiwanie poprzez utworzenie list*/

package Repositories;

import java.util.List;
import domain.User;

public interface IRepositoryUser extends IRepository<User>{

User searchByLogin(String login);
User searchByEmail(String email);
List<User> searchByName(String name);
List<User> searchByCity(String city);
List<User> searchByAge(int age);
List<User> searchBySurname(String surname);
        
}
