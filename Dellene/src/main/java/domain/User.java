package domain;

/**
        Klasa dotyczaca uzytkownikow - ustala ich login, haslo, id..
     */


public class User extends Person {

   
	private String login;
	private String password;
	
        
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
