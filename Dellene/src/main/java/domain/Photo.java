package domain;

/**
        Zdj�cia zwierz�t - tu s� nadawane opisy oraz data zdj�� pupili..
     */

public class Photo extends Entity {

	public String description;
	public int date;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	
	
	
}
